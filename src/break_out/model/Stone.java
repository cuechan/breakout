package break_out.model;

import break_out.Constants;
import break_out.model.Position;
import java.awt.Color;
import java.awt.Rectangle;

public class Stone {
	public static final Color COLOR_1 = new Color(188, 231, 253);
	public static final Color COLOR_2 = new Color(240, 255, 255);
	public static final Color COLOR_3 = new Color(115, 170, 200);

	/**
	 * Integer-Wert aus der Steinemustermatrix. Der Typ bestimmt u.a. wie oft ein
	 * Stein getroffen werden muss, bis er vom Spielfeld entfernt wird.
	 */
	private int type;

	/**
	 * Der Wert zum Ermitteln des Scores. Der Wert kann frei gewählt werden, muss
	 * aber abhängig vom Typ sein.
	 */

	private int value;

	/**
	 * Die Farbe des Steins, abhängig vom Typ.
	 */

	private Color color;

	/**
	 * Die Position auf dem Spielfeld.
	 */
	private Position pos;

	/**
	 * Create a new stone
	 * @param type Type of the stone
	 * @param position Position of the stone
	 */
	public Stone(int type, Position position) {
		this.type = type;
		pos = position;

		switch (type) {
			case 0:
				this.color = null;
				this.value = 0;
				break;
			case 1:
				this.color = COLOR_1;
				this.value = 10;
				break;
			case 2:
				this.color = COLOR_2;
				this.value = 15;
				break;
			case 3:
				this.color = COLOR_3;
				this.value = 25;
				break;
		}
	}


	/**
	 * Get the current position as a rectangle
	 * @return a rectangle
	 */
	public Rectangle getStoneBounds() {
		return new Rectangle(
			(int) this.pos.getX(),
			(int) (this.pos.getY()),
			(int) (Constants.SCREEN_WIDTH / Constants.SQUARES_X),
			(int) (Constants.SCREEN_HEIGHT / Constants.SQUARES_Y) // stones height
		);
	}

	/**
	 * Setter for type
	 * @param type Type of stone
	 */
	public void setType(int type) {
		this.type = type;
	};

	/**
	 * Setter for value
	 * @param value The value of the stone
	 */
	public void setValue(int value) {
		this.value = value;
	};

	/**
	 * Setter for color
	 * @param color Color of the stone
	 */
	public void setColor(Color color) {
		this.color = color;
	};

	/**
	 * Setter for Pos
	 * @param pos Position of the stone
	 */
	public void setPos(Position pos) {
		this.pos = pos;
	};

	/**
	 * Getter for type
	 * @return Type of the stone
	 */
	public int getType() {
		return this.type;
	};

	/**
	 * Getter for value
	 * @return Value of the stone
	 */
	public int getValue() {
		return this.value;
	};

	/**
	 * Getter for color
	 * @return Color of the stone
	 */
	public Color getColor() {
		return this.color;
	};

	/**
	 * Getter for pos
	 * @return Position of the stone
	 */
	public Position getPos() {
		return this.pos;
	};

}
