package break_out.model;

import break_out.Constants;

/**
 * This class represent a game paddle that the user controls.
 * @author Roni Draether
 * @author Paul Maruhn
 */

public class Paddle {

	/**
	 *	paddle moves left
	 */
	public static final int LEFT  = -1;

	/**
	 *	paddle stops
	 */
	public static final int STOP  = 0;

	/**
	 *	paddle moves right
	 */
	public static final int RIGHT = 1;

	/**
	 *	direction of the paddle
	 */
	private int direction = STOP;

	/**
	 *	position of the paddle
	 */
	private Position position;

	/**
	 * This constructor creates a new paddle in the bottom middle of the screen
	 */
	public Paddle() {
		this.position = new Position(
			(Constants.SCREEN_WIDTH - Constants.PADDLE_WIDTH) / 2,
			Constants.SCREEN_HEIGHT - Constants.PADDLE_HEIGHT
		);
	}

	/**
	 * This returns the paddle's current position. Returned position is the beginning of the paddle!
	 * @return current position of the paddle
	 */
	public Position getPosition() {
		return this.position;
	}

	public void updatePosition() {
		this.position.setX(this.position.getX() + direction * Constants.DX_MOVEMENT);

		// if paddle oversteps left border, reset to border contact
		if (this.position.getX() < 0)
			this.position.setX(0);

		// if paddle oversteps right border, reset to border contact
		if (this.position.getX() + Constants.PADDLE_WIDTH > Constants.SCREEN_WIDTH)
			this.position.setX(Constants.SCREEN_WIDTH - Constants.PADDLE_WIDTH);

	}
	/**
	 * Getter for direction of paddle
	 * @return -1 for left\n0 for stop\n1 for right
	 */
	public int getDirection() {
		return this.direction;
	}

	/**
	 * Setter for direction of paddle
	 * @param direction The direction of the paddle (-1 left, 0 stop, 1 right)
	 */
	public void setDirection(int direction) {
		// only accept int from [-1,1]
		if (LEFT <= direction  && direction <= RIGHT)
			this.direction = direction;
	}
}
