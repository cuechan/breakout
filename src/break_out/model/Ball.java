package break_out.model;

import break_out.Constants;
import break_out.model.Position;
import java.awt.Rectangle;
import break_out.model.MatrixPosition;

/**
 * This class contains the information about the balls characteristics and behavior
 *
 * @author iSchumacher, modified by Paul Maruhn and Roni Draether
 */
public class Ball {

	/**
	 * The balls position on the playground
	 */
	private Position position;

	/**
	 * The balls direction
	 */
	private Vector2D direction;


	/**
	 * the stones position, that was hit
	 */
	private MatrixPosition hitStonePosition;

	/**
	 * The constructor of a ball
	 * The balls position and direction are initialized here.
	 */
	public Ball() {
		// x-pos: bisecting screen width and ball diameter to really center the ball
		// y-pos: subtracting  ball diameter and paddle height so the ball lays on the paddle
		this.position = new Position(
			(Constants.SCREEN_WIDTH - Constants.BALL_DIAMETER) / 2,
			Constants.SCREEN_HEIGHT - (Constants.BALL_DIAMETER + Constants.PADDLE_HEIGHT)
		);

		this.direction = new Vector2D(2, -2);

		// normalize ball speed
		direction.rescale();
	}

	/**
	 * The getter for the balls position
	 * @return The balls current position
	 */
	public Position getPosition() {
		return this.position;
	}

	/**
	 * The getter for the balls direction
	 * @return The balls current direction
	 */
	public Vector2D getDirection() {
		return this.direction;
	}

	/**
	 * Calculate the balls new position on the game field
	 */
	public void updatePosition() {
		// add the x-part of the vector to the current x position
		this.position.setX(this.position.getX() + this.direction.getDx());
		// add the y-part of the vector to the current y position
		this.position.setY(this.position.getY() + this.direction.getDy());
	}

	/**
	 * handle ball collision with field borders
	 */
	public void reactOnBorder() {
		// touches or oversteps upper border
		if(this.position.getY() <= 0) {
			// reset to border touch position
			this.position.setY(0);

			// invert y direction
			this.direction.setDy(-this.direction.getDy());
		}

		// touches or oversteps right-hand border
		if(this.position.getX() >= Constants.SCREEN_WIDTH - Constants.BALL_DIAMETER) {
			// reset to border touch position
			this.position.setX(Constants.SCREEN_WIDTH - Constants.BALL_DIAMETER);

			// invert x direction
			this.direction.setDx(-this.direction.getDx());
		}

		// touches or oversteps left-hand border
		if(this.position.getX() <= 0) {
			// reset to border touch position
			this.position.setX(0);

			// invert x direction
			this.direction.setDx(-this.direction.getDx());
		}
	}

	/**
	 * handle ball collision with paddle
	 * @param p The paddle
	 */
	public void reflectOnPaddle(Paddle p) {
		// set ball's direction to vector between offset-point and ball center
		this.direction = new Vector2D(
				new Position(
						// off-set point of the paddle
						p.getPosition().getX() + (Constants.PADDLE_WIDTH / 2),
						p.getPosition().getY() + Constants.REFLECTION_OFFSET
				),
				new Position(
						// center of the ball
						this.getPosition().getX() + Constants.BALL_DIAMETER / 2,
						this.getPosition().getY() + Constants.BALL_DIAMETER / 2
				)
		);

		this.direction.rescale();
	}

	/**
	 * check whether ball hits paddle
	 * @param p the paddle
	 * @return true if paddle was hit, else false
	 */
	public boolean hitsPaddle(Paddle p) {
		// just calculating the respective edges separately,
		// to have a cleaner looking if-statement
		double paddleRightEdge = p.getPosition().getX() + Constants.PADDLE_WIDTH;
		double ballPosition    = this.position.getX() 	+ Constants.BALL_DIAMETER / 2;

		// first check if ball is same height or lower than paddle (happens less often, thus saving compute-time)
		// afterwards check if ball is over the paddle horizontally speaking
		if (this.position.getY() + Constants.BALL_DIAMETER >= p.getPosition().getY()
				&& p.getPosition().getX() <= ballPosition
				&& ballPosition <= paddleRightEdge ) {
			return true;
		}

		return false;

	}

	/**
	 * Checks whether the ball is at or below bottom
	 * @return true: the ball touches or is below bottom screen, false: ball is above bottom
	 */
	public boolean ballLost() {
		// '>' means below, '=' means touching the bottom
		return this.position.getY()+Constants.BALL_DIAMETER>=Constants.SCREEN_HEIGHT;
	}

	/**
	 * Get the current position as a rectangle
	 *
	 * @return a rectangle
	 */
	public Rectangle getBallBounds() {
		return new Rectangle(
			(int) this.position.getX(),
			(int) this.position.getY(),
			(int) Constants.BALL_DIAMETER.doubleValue(),
			(int) Constants.BALL_DIAMETER.doubleValue()
		);
	}

	/**
	 * checks if the collides with a stone
	 * @param stones
	 * @return
	 */

	// This method takes 938 ms (86.8%) of the total cputime in Thread-0
	public boolean hitsStone(Stone[][] stones) {
		for (int row = 0; row < Constants.SQUARES_Y; row++) {

			for (int column = 0; column < Constants.SQUARES_X; column++) {
				if (stones[row][column].getType() != 0 && this.getBallBounds().intersects(stones[row][column].getStoneBounds())) {
					this.reflectOnStone(this.getBallBounds(), stones[row][column].getStoneBounds());
					this.hitStonePosition = new MatrixPosition(column, row);
					return true;
				}
			}
		}

		return false;
	}

	private void reflectOnStone(Rectangle ballRect, Rectangle stoneRect) {
		Rectangle intersection = ballRect.intersection(stoneRect);

		// provoke edge cases
		if (intersection.height <= intersection.width) {
			this.direction.setDy(-this.direction.getDy());
		}
		if (intersection.height > intersection.width) {
			this.direction.setDx(-this.direction.getDx());
		}
	}


	public MatrixPosition getHitStonePosition() {
		return this.hitStonePosition;
	}
}
