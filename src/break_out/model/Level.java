package break_out.model;

import java.awt.Color;

import break_out.Constants;
import break_out.controller.JSONReader;

/**
 * This class contains information about the running game
 *
 * @author dmlux
 * @author I. Schumacher, modified by Paul Maruhn and Roni Draether
 */
public class Level extends Thread {

    /**
     * The game to which the level belongs
     */
    private Game game;

    /**
   	 * The number of the level
   	 */
    private int levelnr;

    /**
	 * The score of the level
	 */
    private int score;

    /**
     * The ball of the level
     */
    private Ball ball;

    /**
     * The paddle controlled by the user to hit the ball
     */
    private Paddle paddle;

    /**
     * Flag that shows if the ball was started
     */
    private boolean ballWasStarted = false;


	/**
     * Flag that shows if the game is finished or running. Used for quitting a game
     */
	private boolean finished = true;

	/**
	 * Stonematrix  (columns x rows)
	 */
	private Stone[][] stones = new Stone[Constants.SQUARES_Y][Constants.SQUARES_X];


	/**
	 * Life counter
	 */
	private int lifeCnt;

    /**
     * The constructor creates a new level object and needs the current game object,
     * the number of the level to be created and the current score
     * @param game The game object
     * @param levelnr The number of the new level object
     * @param score The score
     */
    public Level(Game game, int levelnr, int score) {
    	this.game = game;
    	this.levelnr = levelnr;
    	this.score = score;
        this.ball = new Ball();
		this.paddle = new Paddle();
		this.finished = false;

        loadLevelData(levelnr);
    }

    /**
     * The getter for the ball object
     * @return ball The ball of the level
     */
    public Ball getBall() {
    	return this.ball;
    }

    /**
     * The getter for the paddle object
     * @return paddle The paddle for the user
     */
    public Paddle getPaddle() {
    	return this.paddle;
    }

    /**
     * The getter for the stones array
     * @return stones The stones array
     */
    public Stone[][] getStones() {
    	return this.stones;
    }

    /**
     * The getter for the lifeCnt
     * @return paddle The lifeCnt of the level
     */
    public int getLifeCnt() {
    	return this.lifeCnt;
    }

    /**
     * The getter for the Score
     * @return paddle The Score of the level
     */
    public int getScore() {
    	return this.score;
    }



    /**
     * Finishes the game
     */
	public void setFinished() {
		this.finished = true;
	}

    /**
     * Sets ballWasStarted to true, the ball is moving
     */
    public void startBall() {
        ballWasStarted = true;
    }

    /**
     * Sets ballWasStarted to false, the ball is stopped
     */
    public void stopBall() {
        ballWasStarted = false;
    }

    /**
     * Returns if the ball is moving or stopped
     * @return ballWasStarted true: the ball is moving; false: the ball is stopped
     */
    public boolean ballWasStarted() {
        return ballWasStarted;
	}

	/**
     * Updates the score and value of the stone being hit
     */
    private void updateStonesAndScore() {
    	int col = this.ball.getHitStonePosition().getColumn();
    	int lin = this.ball.getHitStonePosition().getLine();
    	Stone hitStone = stones[col]
			    			[lin];

    	// increase the score by the value of the stone being hit
    	// and reduce the stone's life by 1
    	this.score += hitStone.getValue();
    	hitStone.setType(hitStone.getType()-1);
    	Color newColor = null;
    	switch (hitStone.getType()) {
    	case 1:
    		newColor = Stone.COLOR_1;
    		break;
    	case 2:
    		newColor = Stone.COLOR_2;
    		break;
    	case 3:
    		newColor = Stone.COLOR_3;
    		break;
    	}
    	hitStone.setColor(newColor);
    }

    /**
     * Check whether every stones has been removed from the playground
     * @return true: when all stones have been removed. false: not all stones have been removed
     */
    private boolean allStonesBroken() {
    	for (Stone columns[] : stones) {
    		for (Stone stone : columns) {
    			if (stone.getValue()>0)
    				return false;	// return false when not removed stone is found
    		}
    	}
    	return false;
    }

    /**
     * The method of the level thread
     */
    public void run() {
    	game.notifyObservers();

    	// endless loop
    	while (!finished) {
    		// if ballWasStarted is true, the ball is moving
	        if (ballWasStarted) {

	        	if (this.paddle.getDirection() != Paddle.STOP)
	        		// update the paddles position (if arrow-keys are pressed)
	        		this.paddle.updatePosition();

	        	// Call here balls method for updating the position of the playground
	            this.ball.updatePosition();

	            // Call the method to check whether ball is lost, handle it.
	            if (this.ball.ballLost()) {
	            	decreaseLives();
	            }

	            // Call here the balls method for reacting on the borders of the playground
	        	this.ball.reactOnBorder();

	            // check if ball hits paddle after updatePosition because
	            // the ball touches the paddle at beginning of the game
	        	if (this.ball.hitsPaddle(this.paddle)) {
	        		this.ball.reflectOnPaddle(this.paddle);
	        	}

	        	// if a stone is hit, update stone, score. Check if all stones gone. If so: finish game.
	        	if (this.ball.hitsStone(this.stones)) {
	        		updateStonesAndScore();
	        		if (allStonesBroken()) {
	        			setFinished();
						this.game.getController().toStartScreen();
					}
	        	}

	            // Tells the observer to repaint the components on the playground
	            game.notifyObservers();
	        }
	        // The thread pauses for a short time
	        try {
	            Thread.sleep(4);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
    	}
    }

    /**
    * Loads the information for the level from a json-file located in the folder /res of the project
    * @param levelnr The number X for the LevelX.json file
    */
    private void loadLevelData(int levelnr) {
		JSONReader reader = new JSONReader("res/Level"+1+".json");

		int[][] stoneTypes = reader.getStones2DArray();
		this.lifeCnt 	   = reader.getLifeCounter();


		for (int h = 0; h < stoneTypes.length; h++ ) {

			for (int w = 0; w < stoneTypes[h].length; w++) {
				double x, y;

				x = (Constants.SCREEN_WIDTH / Constants.SQUARES_X) * w;
				y = (Constants.SCREEN_HEIGHT / Constants.SQUARES_Y) * h;


				this.stones[h][w] = new Stone(
					stoneTypes[h][w],
					new Position(x,y)
				);
			}
		}
	}

    /**
     * Decreases life by one. Resets ball/paddle position
     * or finishes game if all lives are lost.
     */
    private void decreaseLives() {
    	this.lifeCnt--;
    	if (this.lifeCnt>0) {
    		// Stop ball and use new ball/paddles as they spawn at the middle.
    		// Let the garbage collector do the rest
    		stopBall();
    		this.paddle = new Paddle();
    		this.ball 	= new Ball();
    	} else {
    		// Finish game and go to start-screen
    		this.setFinished();
    		this.game.getController().toStartScreen();
    	}
    }
}
