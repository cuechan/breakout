package break_out.model;

import break_out.Constants;
import break_out.model.Position;

/**
 * This class represent a two dimensional vector.
 * @author I. Schumacher, modified by Roni Draether, Paul Maruhn
 */
public class Vector2D {

	/**
	 * The x part of the vector
	 */
	private double dx;

	/**
	 * The y part of the vector
	 */
	private double dy;

	/**
	 * This constructor creates a new vector with the given x and y parts.
	 *
	 * @param dx The x part of the vector
	 * @param dy The y part of the vector
	 */
	public Vector2D(double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * This constructor creates a new vector by calculating the dx- and dy-part of the
	 * vector from two positions
	 *
	 * @param a First position of the vector
	 * @param b Second position of the vector
	 */
	public Vector2D(Position a, Position b) {
		// vectors between two points are calculated by subtracting the
		// start-vector from the target-vector (b-a)
		dx = b.getX() - a.getX();
		dy = b.getY() - a.getY();
	}


	/**
	 * sets the vector length to Constants.BALL_SPEED
	 */
	public void rescale() {
		// length of a vector by using Pythagorean theorem
		double length = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

		// vectors are shortened by dividing their components individually
		// multiplying the normed values with BALL_SPEED
		dx = (Constants.BALL_SPEED*dx)/length;
		dy = (Constants.BALL_SPEED*dy)/length;
	}

	/**
	 * Getter for the x part
	 *
	 * @return dx The x part of the vector
	 */
	public double getDx() {
		return dx;
	}

	/**
	 * Setter for the x part
	 *
	 * @param dx The new x part of the vector
	 */
	public void setDx(double dx) {
		this.dx = dx;
	}

	/**
	 * Getter for the y part
	 *
	 * @return dy The y part of the vector
	 */
	public double getDy() {
		return dy;
	}

	/**
	 * Setter for the y part
	 *
	 * @param dy The new y part of the vector
	 */
	public void setDy(double dy) {
		this.dy = dy;
	}

}
