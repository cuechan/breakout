package break_out.model;

/**
 * This class describes a position in the Matrix
 * @author Paul Maruhn and Roni Draether	
 */

public class MatrixPosition {

	private int line;
	private int column;


	/**
	 * Creates a new MatrixPostion
	 * @param line the line (horizontal)
	 * @param column the column (vertical)
	 */
	public MatrixPosition(int line, int column) {
		this.line = line;
		this.column = column;
	}

	/**
	 * returns the line
	 * @return line of the current position
	 */
	public int getLine() {
		return this.line;
	}


	/**
	 * returns the column
	 * @return  column of the current position
	 */
	public int getColumn() {
		return this.column;
	}
}
