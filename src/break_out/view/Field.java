package break_out.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.GlyphVector;

import javax.swing.JPanel;
import break_out.Constants;
import break_out.model.Stone;
import net.miginfocom.swing.MigLayout;

/**
 * The field represents the board of the game. All components are on the board
 *
 * @author dmlux, modified by iSchumacher, Roni Draether and Paul Maruhn
 *
 */
public class Field extends JPanel {

	/**
	 * Automatic generated serial version UID
	 */
	private static final long serialVersionUID = 2434478741721823327L;

	/**
	 * The connected view object
	 */
	private View view;

	/**
	 * The background color
	 */
	private Color background;

	/**
	 * The constructor needs a view
	 *
	 * @param view
	 *            The view of this board
	 */
	public Field(View view) {
		super();

		this.view = view;
		this.background = new Color(20, 20, 20);

		setFocusable(true);

		// Load settings
		initialize();
	}

	/**
	 * Initializes the settings for the board
	 */
	private void initialize() {
		// creates a layout
		setLayout(new MigLayout("", "0[grow, fill]0", "0[grow, fill]0"));
	}

	/**
	 * Change the background color
	 *
	 * @param color
	 *            The new color
	 */
	public void changeBackground(Color color) {
		background = color;
		repaint();
	}

	/**
	 * This method is called when changing/repainting the playground
	 *
	 * @param g
	 *            the graphics object
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		double w = Constants.SCREEN_WIDTH;
		double h = Constants.SCREEN_HEIGHT;

		// Setting the dimensions of the playground
		setPreferredSize(new Dimension((int) w, (int) h));
		setMaximumSize(new Dimension((int) w, (int) h));
		setMinimumSize(new Dimension((int) w, (int) h));

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Setting the background color
		g2.setColor(background);
		g2.fillRect(0, 0, getWidth(), getHeight());

		// Calls the method for drawing the grid (has it's own color selection)
		drawGrid(g2);

		// Setting the color for the following components. CHANGED COLORS (bc old ones
		// were little out-dated)
		g2.setColor(new Color(255, 111, 255));

		// Call the method for drawing the ball, paddle, stones, lives, score
		drawBall(g2);
		drawPaddle(g2);
		drawScore(g2);
		drawLives(g2);
		drawStones(g2); // NOTE! This method changes the g2 color on it's own.

	}

	/**
	 * Draws the ball
	 *
	 * @param g2
	 *            The graphics object
	 */
	private void drawBall(Graphics2D g2) {
		g2.fillOval((int) this.view.getGame().getLevel().getBall().getPosition().getX(),
				(int) this.view.getGame().getLevel().getBall().getPosition().getY(), Constants.BALL_DIAMETER.intValue(),
				Constants.BALL_DIAMETER.intValue());
	}

	/**
	 * Draws the paddle
	 *
	 * @param g2
	 *            The graphics object
	 */
	private void drawPaddle(Graphics2D g2) {
		g2.fillRoundRect((int) this.view.getGame().getLevel().getPaddle().getPosition().getX(), // x coordinate
				(int) this.view.getGame().getLevel().getPaddle().getPosition().getY(), // y coordinate
				Constants.PADDLE_WIDTH.intValue(), // width
				Constants.PADDLE_HEIGHT.intValue(), // height
				5, 5);
	}

	/**
	 * Draws the grid
	 *
	 * @param g2The graphics object
	 */

	// this is one of the methods that is slowing down the game extremely
	// taking 18,108 ms (56.2%) of total time in AWT-EventQueue-0

	private void drawGrid(Graphics2D g2) {
		// Setting the grid color
		g2.setColor(new Color(200, 200, 200));
		int width = (int) (Constants.SCREEN_WIDTH / Constants.SQUARES_X);
		int height = (int) (Constants.SCREEN_HEIGHT / Constants.SQUARES_Y);

		// draw the columns, by drawing SQUARES_X + 1 vertical lines
		for (int i = 0; i <= Constants.SQUARES_X; i++) {
			g2.drawLine(width * i, 0, width * i, Constants.SCREEN_HEIGHT.intValue());

		}

		// draw the rows, by drawing SQUARES_X + 1 horizontal lines
		for (int i = 0; i <= Constants.SQUARES_Y; i++) {
			g2.drawLine(0, height * i, Constants.SCREEN_WIDTH.intValue(), height * i);
		}
	}

	/**
	 * Draws the stones
	 * @param g2 The graphics object
	 */
	private void drawStones(Graphics g2) {
		Stone[][] stones = this.view.getGame().getLevel().getStones();
		for (Stone[] stoneColumn : stones) {
			for (Stone stone : stoneColumn) {

				if (stone.getType() == 0)
					continue;

				// change color for stone that's about to be drawn...
				g2.setColor(stone.getColor());

				// DRAW IT!
				g2.fillRoundRect((int) stone.getPos().getX(), (int) stone.getPos().getY(),
						(int) (Constants.SCREEN_WIDTH / Constants.SQUARES_X) - 1,
						(int) (Constants.SCREEN_HEIGHT / Constants.SQUARES_Y) - 1, 1, 1);
			}
		}
	}

	private void drawScore(Graphics2D g2) {
		g2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 16));
		g2.drawString("SCORE: " + this.view.getGame().getLevel().getScore(), 10, 20);
	}

	private void drawLives(Graphics2D g2) {
		g2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 16));
		g2.drawString("LIVES: " + this.view.getGame().getLevel().getLifeCnt(), 10, 40);
	}
}
