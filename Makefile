PROJECT = BreakOut
SRCDIR = src
BUILDDIR = build
DOCDIR = doc
BINDIR = bin
JARBUILDDIR = jarbuild
MAINCLASS = break_out.Main

JLIBS = libs
JFLAGS = -g -encoding UTF-8
JC = javac
JDOC = javadoc
JDOCFLAGS = -linksource -encoding UTF-8 -header '<a href="https://gitlab.com/cuechan/breakout/commit/$(GITREVLONG)"> <h2>revision $(GITREV) <img src="https://gitlab.com/cuechan/breakout/badges/master/pipeline.svg" /> </h2></a>'

JVM = java
JJAR = jar
JJARFLAGS =

JARFILE = BreakOut.jar
GITREV = $(shell git rev-parse --short HEAD)
GITREVLONG = $(shell git rev-parse HEAD)
SOURCEFILES := $(shell find $(SRCDIR) -name \*.java | cut -sd / -f 2-)
CLASSPATHS := $(shell find $(JLIBS) -name \*.jar | paste -sd :)


.PHONY: all


# pattern rule: tell make how to create a .class file out of a .java file
$(BUILDDIR)/%.class : $(SRCDIR)/%.java
	@echo building $*.java
	$(JC) -cp $(CLASSPATHS) -sourcepath $(SRCDIR) $(JFLAGS) -d $(BUILDDIR) $(SRCDIR)/$*.java


_buildir:
	mkdir -p $(BUILDDIR)


build: classes
	@echo Build finished


classes: _buildir $(SOURCEFILES:%.java=$(BUILDDIR)/%.class)
	@echo All classes compiled


javadoc:
	@echo generating javadoc
	mkdir -p $(DOCDIR)
	$(JDOC) $(JDOCFLAGS) -cp $(CLASSPATHS) -sourcepath $(SRCDIR) -d $(DOCDIR) $(SOURCEFILES:%.java=$(SRCDIR)/%.java)


run: classes
	$(JVM) -Dsun.java2d.opengl=true -cp $(CLASSPATHS):$(BUILDDIR) $(MAINCLASS)


# sketchy things going on here
jar: classes
	@echo start packing $(JARFILE)
	mkdir -p $(JARBUILDDIR)
	cp -r $(BUILDDIR)/* $(JARBUILDDIR)
	unzip -qq -d $(JARBUILDDIR) -o $(JLIBS)/\*.jar -x "META-INF/*"
	$(RM) -r $(JARBUILDDIR)/META-INF
	echo "Main-Class: $(MAINCLASS)" >> $(JARBUILDDIR)/MANIFEST.MF
	$(JJAR) -cef $(MAINCLASS) $(JARFILE) -C $(JARBUILDDIR) .
	$(RM) -r $(JARBUILDDIR)
	@echo $(JARFILE) is ready.


clean: clean_build clean_jar


clean_build:
	@$(RM) -r $(BUILDDIR)/*
	@$(RM) -r $(DOCDIR)/*
	@$(RM) -r $(BINDIR)/*


clean_jar:
	@$(RM) $(JARFILE) latest.jar
